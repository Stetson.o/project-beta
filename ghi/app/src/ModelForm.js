import React, { useState, useEffect } from 'react';

function ModelForm () {
    const [formData, setFormData] = useState({
        name: '',
        picture_url: '',
        manufacturer_id: '',
    });
    const [manufacturers, setManufacturers] =useState([]);

    const getManufacturers = async function() {
        const url = 'http://localhost:8100/api/manufacturers';
        try {
            const res = await fetch(url);
            if(res.ok) {
                const { manufacturers } = await res.json();
                setManufacturers(manufacturers);
            }
        } catch(e) {
            console.log("There was an error fetching manufacturers", e);
        }
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const url = 'http://localhost:8100/api/models/';
        try{
            const fetchOptions = {
                method: 'POST',
                body: JSON.stringify(formData),
                headers: {
                    'Content-Type': 'application/json'
                }
            }
            const res = await fetch(url, fetchOptions);
            if (res.ok) {
                setFormData({
                    name: '',
                    picture_url: '',
                    manufacturer_id: '',
                });
                window.location.reload();
            }
        } catch(e) {
            console.log('An error has occured when POSTing a vechilce model', e);
        }
    }

    useEffect(() => {
        getManufacturers();
    }, []);

    const handleFormChange = function({ target }) {
        const { value, name } = target;

    setFormData({
        ...formData,
        [name]: value
    });
  }

  const {
    name,
    picture_url:pictureUrl,
    manufacturer_id,
  } = formData;

  return (
    <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Add a  Model</h1>
                <form onSubmit={handleSubmit}>

                    <div className="form-floating mb-3">
                    <input value={name} onChange={handleFormChange} type="text" className="form-control" name="name"/>
                    <label htmlFor="name"className="form-label">Name</label>
                    </div>

                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} placeholder="Picture URL"
                        required type="url" value={pictureUrl} name="picture_url" id="picture_url"
                        className="form-control"/>
                        <label htmlFor="picture_url">Picture URL</label>
                    </div>

                    <div className="mb-3">
                        <select onChange={handleFormChange} value={manufacturer_id} required name="manufacturer_id" id="manufacturer_id" className="form-select">
                        <option value="">Choose a Vehicle Model</option>
                        {manufacturers.map(manufacturer => {
                            return (
                            <option key={manufacturer.id} value={manufacturer.id}>
                                {manufacturer.name}
                            </option>
                            );
                        })}
                        </select>
                    </div>



                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
        </div>
    </div>
  )
}


export default  ModelForm;
