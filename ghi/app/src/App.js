import React, { useState, useEffect } from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import AllSalesList from './allsaleslist';
import SalesList from './saleslist';
import SaleForm from './saleform';
import CustomerForm from './customerform';
import SalespersonForm from './salespersonform';
import AutomobileList from './automobilelist';
import AutomobileForm from './automobileform';
import Nav from './Nav';
import ManufacturerForm from './ManufacturerForm';
import ManufacturerList from './ManufacturerList';
import ModelForm from './ModelForm';
import ModelList from './ModelList';
import TechnicianForm from './TechnicianForm';
import TechnicianList from './TechnicianList';
import AppointmentForm from './AppointmentForm';
import AppointmentList from './AppointmentList';
import AppointmentHistory from './AppointmentHistory';


function App() {
  const [technicians, setTechnicians] = useState([]);
  const [appointment, setAppointment] = useState([]);
  const [manufacturers, setManufacturers] = useState([]);
  const [models, setModels] = useState([]);
  const [autos, setAutomobiles] = useState([]);


  const getTechnicians = async () => {
    const technicianUrl = 'http://localhost:8080/api/technicians/'
    const technicianResponse = await fetch(technicianUrl);

    if (technicianResponse.ok) {
      const data = await technicianResponse.json();
      const technicians = data.technicians;
      setTechnicians(technicians);
    }
  }

  const getAppointment = async () => {
    const appointmentUrl = 'http://localhost:8080/api/appointments/'
    const appointmentResponse = await fetch(appointmentUrl);

    if (appointmentResponse.ok) {
      const data = await appointmentResponse.json();
      const appointment = data.appointment;
      setAppointment(appointment);
    }
  }

  const getManufacturers = async () => {
    const manufacturerUrl = 'http://localhost:8100/api/manufacturers/'
    const manufacturerResponse = await fetch(manufacturerUrl);

    if (manufacturerResponse.ok) {
      const data = await manufacturerResponse.json();
      const manufacturers = data.manufacturers;
      setManufacturers(manufacturers);
    }
  }

  const getModels = async () => {
    const modelUrl = 'http://localhost:8100/api/models/'
    const modelResponse = await fetch(modelUrl);

    if (modelResponse.ok) {
      const data = await modelResponse.json();
      const models = data.models;
      setModels(models);
    }
  }

  const getAutomobiles = async () => {
    const response = await fetch("http://localhost:8100/api/automobiles/");

    if (response.ok) {
        const data = await response.json();
        setAutomobiles(data.autos)
    }
  }


  useEffect( () => {
    getTechnicians();
    getAppointment();
    getManufacturers();
    getModels();
    getAutomobiles();
  }, []);

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />

          <Route path="/manufacturers" element={<ManufacturerList manufacturers={manufacturers} getManufacturers={getManufacturers}/>} />
            <Route path="/manufacturers/create" element={<ManufacturerForm />} />

          <Route path="/models" element={<ModelList models={models} getModels={getModels}/>} />
            <Route path="/models/create" element={<ModelForm />} />

          <Route path="/technicians" element={<TechnicianList technicians={technicians} getTechnicians={getTechnicians}/>} />
            <Route path="/technicians/create" element={<TechnicianForm />} />

          <Route path="/appointments" element={<AppointmentList appointment={appointment} setAppointment={setAppointment} autos={autos} getAppointment={getAppointment}/>} />
            <Route path="/appointments/create" element={<AppointmentForm />} />
            <Route path="/appointments/history" element={<AppointmentHistory appointment={appointment}  autos={autos} getAppointment={getAppointment}/>} />

          <Route path="/allsales/" element={<AllSalesList />} />
          <Route path="/sales/" element={<SalesList />} />
          <Route path="/sales/create/" element={<SaleForm />} />
          <Route path="/customer/create/" element={<CustomerForm />} />
          <Route path="/salesperson/create/" element={<SalespersonForm />} />
          <Route path="/automobiles/" element={<AutomobileList />} />
          <Route path="/automobiles/create/" element={<AutomobileForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
