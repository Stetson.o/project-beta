import React from 'react';

function AppointmentList({ appointment, setAppointment, autos }) {

  const updateAppointmentStatus = async (vin, newStatus) => {
    const response = await fetch(`http://localhost:8080/api/appointments/${vin}/`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ status: newStatus }),
    });

    return response.ok;
  };

  const handleStatusChange = async (vin, newStatus) => {
    const isSuccess = await updateAppointmentStatus(vin, newStatus);

    if (isSuccess) {
      const updatedAppointments = appointment.filter(appointmentItem => appointmentItem.vin !== vin);
      setAppointment(updatedAppointments);
      window.location.reload();
    }
  };

  const filteredAppointments = appointment.filter(appointmentItem => (
    appointmentItem.status !== 'Finished' && appointmentItem.status !== 'Canceled'
  ));


  const checkVin = (appointmentVin) => {
    return autos.some(auto => auto.vin === appointmentVin) ? 'Yes' : 'No';
  };

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>VIN</th>
          <th>Is VIP?</th>
          <th>Customer</th>
          <th>Date</th>
          <th>Time</th>
          <th>Technician</th>
          <th>Reason</th>
          <th>Status</th>
        </tr>
      </thead>
      <tbody>
        {filteredAppointments.map(appointmentItem => (
          <tr key={appointmentItem.id}>
            <td>{appointmentItem.vin}</td>
            <td>{checkVin(appointmentItem.vin)}</td>
            <td>{appointmentItem.customer}</td>
            <td>{appointmentItem.customer}</td>
            <td>{new Date(appointmentItem.date_time).toLocaleDateString('en-US', {
              month: '2-digit',
              day: '2-digit',
              year: 'numeric',
            })}</td>
            <td>{new Date(appointmentItem.date_time).toLocaleTimeString('en-US', {
              hour: 'numeric',
              minute: '2-digit',
              second: '2-digit',
              hour12: true,
            })}</td>
            <td>{appointmentItem.technician.first_name + " " + appointmentItem.technician.last_name}</td>
            <td>{appointmentItem.reason}</td>
            <td>
              <button onClick={() => handleStatusChange(appointmentItem.vin, 'Finished')} className="btn btn-primary btn-sm">Finished</button>
              <button onClick={() => handleStatusChange(appointmentItem.vin, 'Canceled')} className="btn btn-danger btn-sm">Canceled</button>
            </td>
        </tr>
        ))}
      </tbody>
    </table>
  );
}

export default AppointmentList;
