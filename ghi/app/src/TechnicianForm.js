import React, { useState } from 'react';


function TechnicianForm () {
    const [formData, setFormData] = useState({
        first_name: '',
        last_name: '',
        employee_id: '',});

    const handleSubmit = async (event) => {
        event.preventDefault();
        const url = 'http://localhost:8080/api/technicians/';
        try{
            const fetchOptions = {
                method: 'POST',
                body: JSON.stringify(formData),
                headers: {
                    'Content-Type': 'application/json'
                }
            }
            const res = await fetch(url, fetchOptions);
            if (res.ok) {
                setFormData({
                    first_name: '',
                    last_name: '',
                    employee_id: '',
                });
                window.location.reload();
            }
        } catch(e) {
            console.log('An error has occured when POSTing a technician', e);
        }
    }

  const handleFormChange = function({ target }) {
    const { value, name } = target;

    setFormData({
        ...formData,
        [name]: value
    });
  }

  const {
    first_name,
    last_name,
    employee_id,
  } = formData;

  return (
    <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a Technician</h1>
                <form onSubmit={handleSubmit}>

                    <div className="form-floating mb-3">
                    <input value={first_name} onChange={handleFormChange} type="text" className="form-control" name="first_name"/>
                    <label htmlFor="first_name"className="form-label">First Name</label>
                    </div>

                    <div className="form-floating mb-3">
                    <input value={last_name} onChange={handleFormChange} type="text" className="form-control" name="last_name"/>
                    <label htmlFor="last_name"className="form-label">Last Name</label>
                    </div>

                    <div className="form-floating mb-3">
                    <input value={employee_id} onChange={handleFormChange} type="text" className="form-control" name="employee_id"/>
                    <label htmlFor="employee_id"className="form-label">Employee ID</label>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
        </div>
    </div>
  )
}


export default TechnicianForm;
