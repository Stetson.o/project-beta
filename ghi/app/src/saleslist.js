import React, {useState, useEffect} from 'react';
function SalesList() {
    const [sales, setSales] = useState([])
    const [FormData, setFormData] = useState({
        salesperson: ''
    })
    const getData = async () => {
        const response = await fetch("http://localhost:8090/api/sales/");
        if (response.ok) {
            const data = await response.json();
            setSales(data.sales)
        }
    }
    useEffect(()=> {
        getData();
    }, [])
    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...FormData,
            [inputName]: value
        });
    }
    let uniqueSales = sales.reduce((unique, sale) => {
        return unique.some(item => item.salesperson.id === sale.salesperson.id) ? unique : [...unique, sale];
    }, []);
    return (
        <>
        <div>
        <select onChange={handleFormChange} value={FormData.salesperson.id} required name="salesperson" id="salesperson" className="form-select">
            <option value="">Choose a salesperson</option>
            {uniqueSales.map(sale => {
                return (
             <option key={sale.salesperson.id} value={sale.salesperson.id}>{ sale.salesperson.first_name } { sale.salesperson.last_name }</option>
                      )
            },
            )}
        </select>
        </div>
        <table>
            <thead>
                <tr>
                    <th>Salesperson</th>
                    <th>Customer</th>
                    <th>VIN</th>
                    <th>Price</th>
                </tr>
            </thead>
            <tbody>
                {sales.filter(sale => sale.salesperson.id == FormData.salesperson).map(filteredSale => {
                    console.log(filteredSale);return (
                        <tr key={filteredSale.id}>
                            <td>{ filteredSale.salesperson.first_name } {filteredSale.salesperson.last_name }</td>
                            <td>{ filteredSale.salesperson.employee_id }</td>
                            <td>{ filteredSale.customer.first_name } {filteredSale.customer.last_name}</td>
                            <td>{ filteredSale.automobile.vin }</td>
                            <td> ${ filteredSale.price }</td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
        </>
    );
}
export default SalesList;
