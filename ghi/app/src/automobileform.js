import React, { useState, useEffect } from 'react';

function AutomobileForm () {
    const [formData, setFormData] = useState({
        color: '',
        year: '',
        vin: '',
        model_id: '',
    });
    const [models, setModels] = useState([]);

    const getModels = async () => {
        const url = 'http://localhost:8100/api/models';
            const res = await fetch(url);

            if(res.ok) {
                const data = await res.json();
                setModels(data.models);
            }
        }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const url = 'http://localhost:8100/api/automobiles/';
        const fetchOptions = {
            method: 'POST',
            body: JSON.stringify(formData),
            headers: {
                  'Content-Type': 'application/json',
            },
        };
        const res = await fetch(url, fetchOptions);
        if (res.ok) {
            setFormData({
                color: '',
                year: '',
                vin: '',
                model_id: '',
            });
            window.location.reload();
        }
    }

    useEffect(() => {
        getModels();
    }, []);

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;
    setFormData({
        ...formData,
        [inputName]: value
    });
  }

  return (
    <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Add an Automobile</h1>
                <form onSubmit={handleSubmit}>

                    <div className="form-floating mb-3">
                    <input value={formData.color} onChange={handleFormChange} type="text" className="form-control" name="color"/>
                    <label htmlFor="color"className="form-label">Color</label>
                    </div>

                    <div className="form-floating mb-3">
                    <input value={formData.year} onChange={handleFormChange} type="number" className="form-control" name="year"/>
                    <label htmlFor="year"className="form-label">Year</label>
                    </div>

                    <div className="form-floating mb-3">
                    <input value={formData.vin} onChange={handleFormChange} type="text" className="form-control" name="vin"/>
                    <label htmlFor="vin"className="form-label">Vin</label>
                    </div>

                    <div className="mb-3">
                        <select onChange={handleFormChange} value={formData.model_id} required name="model_id" id="model_id" className="form-select">
                        <option value="">Choose a model</option>
                        {models.map(model => {
                            return (
                            <option key={model.id} value={model.id}>
                                {model.name}
                            </option>
                            );
                        })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
        </div>
    </div>
  )
}


export default  AutomobileForm;
