import React from 'react';

function  ModelList({models}) {
    if (models === undefined) {
        return null;
      }

    return (

        <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
          </tr>
        </thead>
        <tbody>
          {models.map(model => {
            return (
            <tr key={model.id}>
                <td>{ model.name }</td>
                <td>
                <img src={ model.picture_url } alt='' width="100px" height="100px" />
                </td>
                <td>{ model.name }</td>
            </tr>
            );
          })}
          </tbody>
      </table>
    );
}


export default ModelList;
