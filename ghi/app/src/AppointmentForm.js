import React, { useState, useEffect } from 'react';

function AppointmentForm () {
    const [formData, setFormData] = useState({
        reason: '',
        status: '',
        vin: '',
        customer: '',
        technician_id: '',
        date: '',
        time: '',
    });
    const [technicians, setTechnicians] =useState([]);

    const getTechnicians = async function() {
        const url = 'http://localhost:8080/api/technicians';
        try {
            const res = await fetch(url);
            if(res.ok) {
                const { technicians } = await res.json();
                setTechnicians(technicians);
            }
        } catch(e) {
            console.log("There was an error fetching technicians", e);
        }
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const dateTime = `${formData.date}T${formData.time}`;

        const submissionData = {
            ...formData,
            date_time: dateTime,
            status: "Created",
        };

        delete submissionData.date;
        delete submissionData.time;


        const url = 'http://localhost:8080/api/appointments/';
        try{
            const fetchOptions = {
                method: 'POST',
                body: JSON.stringify(submissionData),
                headers: {
                    'Content-Type': 'application/json'
                }
            }
            const res = await fetch(url, fetchOptions);
            if (res.ok) {
                setFormData({
                    reason: '',
                    status: '',
                    vin: '',
                    customer: '',
                    technician_id: '',
                    date: '',
                    time: '',
                });
                window.location.reload();
            }
        } catch(e) {
            console.log('An error has occured when POSTing a appointment', e);
        }
    }

    useEffect(() => {
        getTechnicians();
    }, []);

    const handleFormChange = function({ target }) {
        const { value, name } = target;

    setFormData({
        ...formData,
        status: "Created",
        [name]: value
    });
  }

  const {
    vin,
    customer,
    technician_id,
    reason,
  } = formData;

  return (
    <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a service appointment</h1>
                <form onSubmit={handleSubmit}>
                    <div className="form-group mb-3">
                    <label htmlFor="vin"className="form-label">Automobile VIN</label>
                    <input value={vin} onChange={handleFormChange} type="text" className="form-control" name="vin"/>
                    </div>
                    <div className="form-group mb-3">
                    <label htmlFor="customer"className="form-label">Customer</label>
                    <input value={customer} onChange={handleFormChange} type="text" className="form-control" name="customer"/>
                    </div>
                    <div className="form-group mb-3">
                            <label htmlFor="date" className="form-label">Date</label>
                            <input value={formData.date} onChange={handleFormChange} type="date" className="form-control" name="date" id="date" />
                        </div>
                        <div className="form-group mb-3">
                            <label htmlFor="time" className="form-label">Time</label>
                            <input value={formData.time} onChange={handleFormChange} type="time" className="form-control" name="time" id="time" />
                        </div>
                    <div className="mb-3">
                        <label htmlFor="technician_id"className="form-label">Technician</label>
                        <select onChange={handleFormChange} value={technician_id} required name="technician_id" id="technician_id" className="form-select">
                        <option value="">Choose a technician</option>
                        {technicians.map(technician => {
                            return (
                            <option key={technician.id} value={technician.id}>
                                { technician.first_name + " " + technician.last_name }
                            </option>
                            );
                        })}
                        </select>
                    </div>
                    <div className="form-group mb-3">
                    <label htmlFor="reason"className="form-label">Reason</label>
                    <input value={reason} onChange={handleFormChange} type="text" className="form-control" name="reason"/>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
        </div>
    </div>
  )
}


export default  AppointmentForm;
