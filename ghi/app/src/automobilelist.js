import React, { useEffect, useState } from 'react';

function  AutomobileList() {
    const [automobile, setAutomobile] = useState([])

    const getData = async () => {
        const response = await fetch("http://localhost:8100/api/automobiles/");

        if (response.ok) {
            const data = await response.json();
            setAutomobile(data.autos)
        }
    }

    useEffect(()=>{
        getData();
    }, [])
    return (

        <table className="table table-striped">
        <thead>
          <tr>
            <th>Color</th>
            <th>Year</th>
            <th>vin</th>
            <th>Model</th>
            <th>Sold</th>
          </tr>
        </thead>
        <tbody>
          {automobile.map(autos => {
            return (
            <tr key={autos.id}>
                <td>{ autos.color }</td>
                <td>{ autos.year }</td>
                <td>{ autos.vin }</td>
                <td>{ autos.model.name }</td>
                <td>{ autos.sold ? "Yes" : "No"}</td>
            </tr>
            );
          })}
          </tbody>
      </table>
    );
}


export default AutomobileList;
