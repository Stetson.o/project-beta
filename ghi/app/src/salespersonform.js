import React, {useState} from 'react';

function SalespersonForm() {
    const [FormData, setFormData] = useState({
        first_name: "",
        last_name: "",
        employee_id: "",
    })
    const handleSubmit = async (event) => {
        event.preventDefault();
        const salespeopleUrl = "http://localhost:8090/api/salespeople/"

        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(FormData),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const response = await fetch(salespeopleUrl, fetchConfig);

        if (response.ok) {
            setFormData({
                first_name: "",
                last_name: "",
                employee_id: "",
            })
        }
    }
    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...FormData,
            [inputName]: value
        });
    }

    return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a new salesperson</h1>
              <form onSubmit={handleSubmit} id="create-hat-form">
                <div className="form-floating mb-3">
                  <input onChange={handleFormChange} value={FormData.first_name} placeholder="First_name" required type="text" name="first_name" id="first_name" className="form-control" />
                  <label htmlFor="first_name">First_name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleFormChange} value={FormData.last_name} placeholder="Last_name" required type="text" name="last_name" id="last_name" className="form-control" />
                  <label htmlFor="last_name">Last_name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleFormChange} value={FormData.employee_id} placeholder="Employee_id" required type="text" name="employee_id" id="employee_idb" className="form-control" />
                  <label htmlFor="employee_id">Employee_id</label>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      );
}
export default SalespersonForm;
