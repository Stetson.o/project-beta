import React from 'react';

function ManufacturerList({manufacturers}) {
    if (manufacturers === undefined) {
        return null;
      }

    return (

        <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
          </tr>
        </thead>
        <tbody>
          {manufacturers.map(manufacturer => {
            console.log(manufacturer)
            return (
            <tr key={manufacturer.id}>
              <td>{ manufacturer.name }</td>
            </tr>
            );
          })}
          </tbody>
      </table>
    );
}


export default ManufacturerList;
