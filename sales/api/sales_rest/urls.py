from django.urls import path
from .views import api_list_salesperson, api_list_customer, api_list_sales


urlpatterns = [
    path("salespeople/", api_list_salesperson, name="api_list_salespeople"),
    path("customers/", api_list_customer, name="api_list_customers"),
    path("sales/", api_list_sales, name="api_list_sales")
]
